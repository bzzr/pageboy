@echo off

set common_compiler_flags=/nologo /Gm- /GR- /GS- /EHa- /Oi /FC /WX /W4
set common_compiler_flags=%common_compiler_flags% /wd4201 /wd4100 /wd4189 /wd4701 /wd4459 /wd4456 /wd4706 /wd4146 /wd4505 /wd4703 /wd4533 /wd4312
set common_compiler_flags=%common_compiler_flags% /DPAGEBOY_INTERNAL=1 /DPAGEBOY_DEBUG=1 /DPAGEBOY_WIN32=1

set common_linker_flags=/incremental:no /opt:ref

set libs=winmm.lib

IF NOT EXIST build mkdir build
IF NOT EXIST dist  mkdir dist

pushd build

cl %common_compiler_flags% /Od /Z7 ..\src\win32_pageboy.cpp ..\src\pageboy.cpp /link %common_linker_flags% %libs%
cl /P ..\src\win32_pageboy.cpp > nul 2>&1
cl /P ..\src\pageboy.cpp       > nul 2>&1

copy win32_pageboy.exe ..\dist\pageboy.exe > nul 2>&1

popd

echo Done.
