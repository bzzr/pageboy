internal uma
round_up(uma n, uma multiple)
{
    uma result = n;
    if (multiple == 0)
    {
        return result;
    }

    uma remainder = n % multiple;
    if (remainder == 0)
    {
        return result;
    }

    result = n + multiple - remainder;
    return result;
}

internal uma
string_length(char *string)
{
    uma size = 0;
    char *at = string;
    while (*at)
    {
        size++;
        at++;
    }
    return size;
}

internal char *
copy_string(char *dest, char *source)
{
    while (true)
    {
        if (*source == 0)
        {
            *dest = 0;
            break;
        }
        *dest++ = *source++;
    }

    return dest;
}

internal b32
compare_strings(char *string1, char *string2, char **end = 0)
{
    char *at1 = string1;
    char *at2 = string2;

    b32 result = true;
    while (*at1 != '\0' && *at2 != '\0')
    {
        if (*at1 != *at2)
        {
            result = false;
            break;
        }
        at1++;
        at2++;
    }

    if (end)
    {
        *end = result ? at1 : string1;
    }

    return result;
}

internal void
reverse_string(char *dest, char *string, uma size)
{
    uma start = 0;
    uma end = size - 1;
    while (start < end)
    {
        char tmp = *(string + start);
        *(dest + start) = *(string + end);
        *(dest + end) = tmp;
        start++;
        end--;
    }
}

internal inline b32
is_digit(char c)
{
    b32 result = c >= 48 && c <= 57;
    return result;
}

internal u32
string_to_u32(char *string, char **end)
{
    u32 result = 0;
    for (; is_digit(*string); string++)
    {
        char c = *string;
        result = result * 10 + *string - '0';
    }

    if (end)
    {
        *end = string;
    }

    return result;
}

internal void
u32_to_string(char *dest, u32 value, char **end)
{
    char *p = dest;
    uma size = 0;
    do
    {
        char digit_index = value % 10;
        char digit = '0' + (char)digit_index;
        *p++ = digit;
        value /= 10;
        size++;
    } while (value != 0);

    reverse_string(dest, dest, size);
    if (end) *end = p;
}

internal char *
linesep_inc(char *str)
{
    char *result = str;
    if (compare_strings(result, (char *)platform.linesep.data))
    {
        result += platform.linesep.count;
    }
    else if (*result == '\n')
    {
        result += 1;
    }
    return result;
}

internal Buffer
advance_buffer(Buffer buffer, uma count)
{
    assert((s32)buffer.count - count >= 0);

    Buffer result;
    result.data  = buffer.data  + count;
    result.count = buffer.count - count;
    return result;
}

internal Buffer
retreat_buffer(Buffer buffer, uma count)
{
    Buffer result;
    result.data  = buffer.data  - count;
    result.count = buffer.count + count;
    return result;
}
