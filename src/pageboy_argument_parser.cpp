#define ARGUMENT_PARSER_MAX_POSITIONAL_PARAMETERS 128
#define ARGUMENT_PARSER_MAX_OPTIONAL_PARAMETERS   128

enum Parameter_Type
{
    PARAMETER_TYPE_BOOL,
    PARAMETER_TYPE_STRING
};

struct Parameter
{
    char *short_name;
    char *long_name;
    void *out;
    Parameter_Type type;
};

struct Argument_Parser
{
    Parameter positional_parameters[ARGUMENT_PARSER_MAX_POSITIONAL_PARAMETERS];
    u32 positional_parameter_count;

    Parameter optional_parameters[ARGUMENT_PARSER_MAX_OPTIONAL_PARAMETERS];
    u32 optional_parameter_count;
};

internal Parameter *
get_optional_parameter_with_short_name(Argument_Parser *parser, char *name)
{
    for (u32 i = 0; i < parser->optional_parameter_count; i++)
    {
        Parameter *parameter = &parser->optional_parameters[i];
        if (compare_strings(parameter->short_name, name))
        {
            return parameter;
        }
    }

    return 0;
}

internal Parameter *
get_optional_parameter_with_long_name(Argument_Parser *parser, char *name)
{
    for (u32 i = 0; i < parser->optional_parameter_count; i++)
    {
        Parameter *parameter = &parser->optional_parameters[i];
        if (compare_strings(parameter->long_name, name))
        {
            return parameter;
        }
    }

    return 0;
}

enum Token_Type
{
    TOKEN_TYPE_POSITIONAL_PARAMETER,
    TOKEN_TYPE_OPTIONAL_PARAMETER_SHORT,
    TOKEN_TYPE_OPTIONAL_PARAMETER_LONG
};

internal inline Token_Type
get_token_type(char *string)
{
    if (string_length(string) == 2 && string[0] == '-')
    {
        return TOKEN_TYPE_OPTIONAL_PARAMETER_SHORT;
    }
    if (string_length(string) > 2 && string[0] == '-' && string[1] == '-')
    {
        return TOKEN_TYPE_OPTIONAL_PARAMETER_LONG;
    }
    return TOKEN_TYPE_POSITIONAL_PARAMETER;
}

internal void
add_positional_parameter(
    Argument_Parser *parser,
    Parameter_Type type,
    void *out)
{
    assert(out);
    assert(parser->positional_parameter_count + 1 < ARGUMENT_PARSER_MAX_POSITIONAL_PARAMETERS);

    Parameter *parameter = &parser->positional_parameters[parser->positional_parameter_count++];
    parameter->type = type;
    parameter->out = out;
}

internal void
add_optional_parameter(
    Argument_Parser *parser,
    char *short_name,
    char *long_name,
    Parameter_Type type,
    void *out)
{
    assert((short_name || long_name) && out);
    assert(!short_name || get_token_type(short_name) == TOKEN_TYPE_OPTIONAL_PARAMETER_SHORT);
    assert(!long_name  || get_token_type(long_name)  == TOKEN_TYPE_OPTIONAL_PARAMETER_LONG);
    assert(!short_name || !get_optional_parameter_with_short_name(parser, short_name));
    assert(!long_name  || !get_optional_parameter_with_long_name(parser, long_name));
    assert(parser->optional_parameter_count + 1 < ARGUMENT_PARSER_MAX_OPTIONAL_PARAMETERS);

    Parameter *parameter = &parser->optional_parameters[parser->optional_parameter_count++];
    parameter->type = type;
    parameter->short_name = short_name;
    parameter->long_name = long_name;
    parameter->out = out;
}

internal void
parse_arguments(Argument_Parser *parser, u32 argc, char **argv)
{
    u32 num_parsed_positional_parameters = 0;
    for (u32 i = 0; i < argc; i++)
    {
        char *argument = argv[i];
        Parameter *parameter;

        Token_Type token_type = get_token_type(argument);
        switch (token_type)
        {
            case TOKEN_TYPE_OPTIONAL_PARAMETER_SHORT:
            {
                parameter = get_optional_parameter_with_short_name(parser, argument);
            } break;
            case TOKEN_TYPE_OPTIONAL_PARAMETER_LONG:
            {
                parameter = get_optional_parameter_with_long_name(parser, argument);
            } break;
            case TOKEN_TYPE_POSITIONAL_PARAMETER:
            {
                if (i < parser->positional_parameter_count)
                {
                    parameter = &parser->positional_parameters[num_parsed_positional_parameters++];
                }
            } break;
        }

        if (!parameter)
        {
            continue;
        }

        switch (parameter->type)
        {
            case PARAMETER_TYPE_BOOL:
            {
                cast_and_deref(parameter->out, b32) = true;
            } break;
            case PARAMETER_TYPE_STRING:
            {
                cast_and_deref(parameter->out, char *) = argument;
            } break;
        }
    }
}
