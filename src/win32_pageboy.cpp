#include <stdio.h>
#include <windows.h>

#include "pageboy_first.cpp"
#include "pageboy_shared.cpp"
#include "pageboy_hid.cpp"

Platform platform;

#include "pageboy_utils.cpp"
#include "pageboy_memory.cpp"


global HANDLE console_in;
global HANDLE console_out;
global HANDLE standard_out;
global UINT prev_code_page;
global s64 performance_counter_frequency;
global int running;

global const wchar_t LONG_PATH_PREFIX[] = {L'\\', L'\\', L'?', '\\'};
global const u32 MAX_EVENTS = 1024;

PLATFORM_ALLOC(win32_alloc)
{
    void *result = VirtualAlloc(0, size, MEM_RESERVE|MEM_COMMIT, PAGE_READWRITE);
    return result;
}

internal b32
win32_free(void *memory)
{
    b32 result = VirtualFree(memory, 0, MEM_RELEASE);
    return result;
}

PLATFORM_RESERVE_MEMORY(win32_reserve_memory)
{
    void *result = VirtualAlloc(0, size, MEM_RESERVE, PAGE_READWRITE);
    return result;
}

PLATFORM_COMMIT_MEMORY(win32_commit_memory)
{
    void *result = VirtualAlloc(address, size, MEM_COMMIT, PAGE_READWRITE);
    return result;
}

PLATFORM_WRITE_TO_STDOUT(win32_write_to_stdout)
{
    size = size ? size : string_length(string);

    DWORD console_mode;
    DWORD written;
    if (GetFileType(standard_out) & FILE_TYPE_CHAR
        && GetConsoleMode(standard_out, &console_mode))
    {
        WriteConsole(console_out, string, (DWORD)size, &written, 0);
    }
    else
    {
        WriteFile(standard_out, string, (DWORD)size, &written, 0);
    }
}

PLATFORM_PANIC(win32_panic)
{
    SetConsoleOutputCP(prev_code_page);
    win32_write_to_stdout(message, 0);
    ExitProcess(1);
}

PLATFORM_GET_FILE_SIZE(win32_get_file_size)
{
    u32 wide_path_size = MultiByteToWideChar(CP_UTF8, 0, path, -1, 0, 0);
    wchar_t *wide_path = (wchar_t *)win32_alloc(wide_path_size * sizeof(wchar_t));
    if (!wide_path)
    {
        return 0;
    }
    MultiByteToWideChar(CP_UTF8, 0, path, -1, wide_path, wide_path_size);

    int result = 0;

    HANDLE file_handle = CreateFileW(wide_path, GENERIC_READ, FILE_SHARE_READ, 0, OPEN_EXISTING, 0, 0);
    if (file_handle != INVALID_HANDLE_VALUE)
    {
        LARGE_INTEGER file_size;
        if (GetFileSizeEx(file_handle, &file_size))
        {
            assert(file_size.QuadPart <= 0xFFFFFFFF);

            *size = (u32)file_size.QuadPart;
            result = 1;
        }
        CloseHandle(file_handle);
    }

    if (!win32_free(wide_path))
    {
        result = 0;
    }

    return result;
}

PLATFORM_READ_FILE(win32_read_file)
{
    u32 wide_path_size = MultiByteToWideChar(CP_UTF8, 0, path, -1, 0, 0);
    wchar_t *wide_path = (wchar_t *)win32_alloc(wide_path_size * sizeof(wchar_t));
    if (!wide_path)
    {
        return 0;
    }
    MultiByteToWideChar(CP_UTF8, 0, path, -1, wide_path, wide_path_size);

    int result = 0;

    HANDLE file_handle = CreateFileW(wide_path, GENERIC_READ, FILE_SHARE_READ, 0, OPEN_EXISTING, 0, 0);
    if (file_handle != INVALID_HANDLE_VALUE)
    {
        DWORD bytes_read;
        if (ReadFile(file_handle, memory, (DWORD)size, &bytes_read, 0) && size == bytes_read)
        {
            result = 1;
        }
        CloseHandle(file_handle);
    }

    if (!win32_free(wide_path))
    {
        result = 0;
    }

    return result;
}

// @Incomplete: Get rid of this.
PLATFORM_WRITE_FILE(win32_write_file)
{
    b32 result = 0;

    HANDLE file_handle = CreateFileA(path, GENERIC_WRITE, FILE_SHARE_READ, 0, CREATE_ALWAYS, 0, 0);
    if (file_handle != INVALID_HANDLE_VALUE)
    {
        DWORD bytes_written;
        if (WriteFile(file_handle, memory, (DWORD)size, &bytes_written, 0))
        {
            result = true;
        }
        CloseHandle(file_handle);
    }

    return result;
}

PLATFORM_ABSOLUTE_PATH(win32_absolute_path)
{
    u32 wide_path_size = MultiByteToWideChar(CP_UTF8, 0, path, -1, 0, 0);
    wchar_t *wide_path = (wchar_t *)win32_alloc(wide_path_size * sizeof(wchar_t));
    if (!wide_path)
    {
        return 0;
    }
    MultiByteToWideChar(CP_UTF8, 0, path, -1, wide_path, wide_path_size);

    u32 wide_absolute_path_size = GetFullPathNameW(wide_path, 0, 0, 0);
    wide_absolute_path_size += array_count(LONG_PATH_PREFIX);
    wchar_t *wide_absolute_path = (wchar_t *)win32_alloc(wide_absolute_path_size * sizeof(wchar_t));
    if (!wide_absolute_path)
    {
        return 0;
    }
    wide_absolute_path = (wchar_t *)copy_memory(wide_absolute_path, (void *)LONG_PATH_PREFIX, array_count(LONG_PATH_PREFIX) * sizeof(wchar_t));
    GetFullPathNameW(wide_path, wide_absolute_path_size, wide_absolute_path, 0);

    u32 absolute_path_size = WideCharToMultiByte(CP_UTF8, 0, wide_absolute_path, -1, 0, 0, 0, 0);
    *absolute_path = (char *)win32_alloc(absolute_path_size);
    if (!(*absolute_path))
    {
        return 0;
    }
    WideCharToMultiByte(CP_UTF8, 0, wide_absolute_path, -1, *absolute_path, absolute_path_size, 0, 0);

    if (!win32_free(wide_path))
    {
        return 0;
    }

    if (!win32_free(wide_absolute_path))
    {
        return 0;
    }

    return 1;
}

internal int
wide_to_utf8_arguments(u32 wargc, wchar_t **wargv, u32 *argc, char ***argv)
{
    *argc = 0;
    u32 argument_data_size = 0;
    for (u32 i = 0; i < wargc; i++)
    {
        argument_data_size += WideCharToMultiByte(CP_UTF8, 0, wargv[i], -1, 0, 0, 0, 0);
        (*argc)++;
    }

    *argv = (char **)win32_alloc(*argc * sizeof(char *));
    if (!(*argv))
    {
        return 0;
    }

    char *data = (char *)win32_alloc(argument_data_size);
    if (!data)
    {
        return 0;
    }

    u32 written = 0;
    for (u32 i = 0; i < *argc; i++)
    {
        u32 argument_size = WideCharToMultiByte(CP_UTF8, 0, wargv[i], -1, 0, 0, 0, 0);
        char *argument = &data[written];
        written += argument_size;
        WideCharToMultiByte(CP_UTF8, 0, wargv[i], -1, argument, argument_size, 0, 0);
        (*argv)[i] = argument;
    }

    return 1;
}

internal LONGLONG
win32_get_timestamp()
{
    LARGE_INTEGER li;
    QueryPerformanceCounter(&li);
    LONGLONG result = li.QuadPart;
    return result;
}

internal f32
win32_get_seconds_elapsed(LONGLONG start, LONGLONG end)
{
    f32 result = (f32)(end - start) / (f32)performance_counter_frequency;
    return result;
}

internal inline LONGLONG
win32_seconds_to_ticks(f32 seconds)
{
    LONGLONG result = (LONGLONG)(seconds * performance_counter_frequency);
    return result;
}

internal void
win32_sleep(f32 seconds)
{
    LONGLONG ticks_to_sleep = win32_seconds_to_ticks(seconds);
    LONGLONG ticks_left = ticks_to_sleep;

    while (ticks_left > 0)
    {
        LONGLONG start = win32_get_timestamp();
        if (ticks_left > win32_seconds_to_ticks(0.002f))
        {
            Sleep(1);
        }
        else
        {
            for (u32 i = 0; i < 10; i++)
            {
                Sleep(0);
            }
        }
        ticks_left -= (win32_get_timestamp() - start);
    }
}

internal b32
win32_get_file_size(wchar_t *file_path, u32 *size)
{
    b32 result = false;

    HANDLE file_handle = CreateFileW(file_path, GENERIC_READ, FILE_SHARE_READ, 0, OPEN_EXISTING, 0, 0);
    if (file_handle != INVALID_HANDLE_VALUE)
    {
        LARGE_INTEGER file_size;
        if (GetFileSizeEx(file_handle, &file_size))
        {
            // @Note Make sure file size is less than (2^32)-1 bytes.
            assert(file_size.QuadPart <= 0xFFFFFFFF);

            *size = (u32)file_size.QuadPart;
            result = true;
        }
        CloseHandle(file_handle);
    }

    return result;
}

#define push_event(event_buffer) \
    (Event *)(&((event_buffer)->data[(event_buffer)->count++ * sizeof(Event)]));

internal void
win32_process_console_events(HANDLE console_in, Buffer *event_buffer, b32 *running)
{
    DWORD num_events;
    if (GetNumberOfConsoleInputEvents(console_in, &num_events))
    {
        for (u32 i = 0; i < num_events; i++)
        {
            INPUT_RECORD record;
            DWORD events_read = 0;
            if (ReadConsoleInput(console_in, &record, 1, &events_read))
            {
                switch (record.EventType)
                {
                    case KEY_EVENT:
                    {
                        Event *event = push_event(event_buffer);

                        event->type = KEYBOARD_EVENT;
                        if (record.Event.KeyEvent.bKeyDown)
                        {
                            event->key.type = KEYBOARD_EVENT_KEYDOWN;
                        }
                        else
                        {
                            event->key.type = KEYBOARD_EVENT_KEYUP;
                        }

                        int key_code = record.Event.KeyEvent.wVirtualScanCode;
                        if (record.Event.KeyEvent.dwControlKeyState & ENHANCED_KEY)
                        {
                            key_code = key_code | 0x80;
                        }
                        event->key.hid_code = (HID_Code)WINDOWS_KEYCODE_TO_HID[key_code];
                    } break;
                }
            }
            else
            {
            }
        }
    }
}

internal Screen_Dimension
win32_get_screen_dimension()
{
    CONSOLE_SCREEN_BUFFER_INFO buffer_info;
    GetConsoleScreenBufferInfo(console_out, &buffer_info);

    Screen_Dimension result;
    result.width  = (buffer_info.srWindow.Right  - buffer_info.srWindow.Left) + 1;
    result.height = (buffer_info.srWindow.Bottom - buffer_info.srWindow.Top)  + 1;

    return result;
}

#define win32_screen_dimension_is_valid(dim) ((dim).width > 0 && (dim).height > 0)
#define win32_compare_screen_dimensions(dim1, dim2) \
    ((dim1).width  == (dim2).width && \
     (dim1).height == (dim2).height)


int wmain(int wargc, wchar_t **wargv, wchar_t envp)
{
    platform.alloc           = win32_alloc;
    platform.reserve_memory  = win32_reserve_memory;
    platform.commit_memory   = win32_commit_memory;
    platform.get_file_size   = win32_get_file_size;
    platform.read_file       = win32_read_file;
    platform.write_to_stdout = win32_write_to_stdout;
    platform.panic           = win32_panic;
    platform.write_file      = win32_write_file;
    platform.absolute_path   = win32_absolute_path;

    platform.linesep.data  = (u8 *)"\r\n";
    platform.linesep.count = 2;

    SYSTEM_INFO system_info;
    GetSystemInfo(&system_info);
    platform.page_size = system_info.dwPageSize;
    platform.allocation_granularity = system_info.dwAllocationGranularity;

    u32 argc;
    char **argv;
    if (!wide_to_utf8_arguments(wargc, wargv, &argc, &argv))
    {
        return 1;
    }

    prev_code_page = GetConsoleOutputCP();

    console_in = CreateFile("CONIN$", GENERIC_READ | GENERIC_WRITE, FILE_SHARE_READ, 0, OPEN_EXISTING, 0, 0);
    console_out = CreateFile("CONOUT$", GENERIC_READ | GENERIC_WRITE, FILE_SHARE_WRITE, 0, OPEN_EXISTING, 0, 0);
    standard_out = GetStdHandle(STD_OUTPUT_HANDLE);
    if (!console_in || !console_out || !standard_out)
    {
        return 1;
    }

    DWORD console_out_mode;
    GetConsoleMode(console_out, &console_out_mode);
    console_out_mode |= ENABLE_VIRTUAL_TERMINAL_PROCESSING;
    console_out_mode &= ~ENABLE_WRAP_AT_EOL_OUTPUT;
    SetConsoleMode(console_out, console_out_mode);

    LARGE_INTEGER query_performance_frequency_result;
    QueryPerformanceFrequency(&query_performance_frequency_result);
    performance_counter_frequency = query_performance_frequency_result.QuadPart;

    MMRESULT time_begin_period_result = timeBeginPeriod(1);
    if (time_begin_period_result != TIMERR_NOERROR) 
    {
        return 1;
    }

    u32 monitor_refresh_rate = 60; // @Note: Query this.
    f32 app_update_rate = monitor_refresh_rate / 2.0f;
    f32 target_seconds_per_frame = 1.0f / (f32)app_update_rate;

    uma event_memory_size = MAX_EVENTS * sizeof(Event);
    void *event_memory = win32_alloc(event_memory_size);
    if (!event_memory)
    {
        win32_write_to_stdout("error: not enough memory\n", 0);
        goto exit;
    }

    Buffer event_buffer;
    event_buffer.data = (u8 *)event_memory;
    event_buffer.count = 0;

    uma permanent_memory_size = megabytes(1);
    void *permanent_memory = win32_alloc(permanent_memory_size);
    if (!permanent_memory)
    {
        win32_write_to_stdout("error: not enough memory\n", 0);
        goto exit;
    }

    Screen_Dimension last_screen_dimension = win32_get_screen_dimension();
    LONGLONG last_timestamp = win32_get_timestamp();

    SetConsoleOutputCP(CP_UTF8);

    running = 1;
    while (running)
    {
        Screen_Dimension screen_dimension = win32_get_screen_dimension();

        // @Note: Test if this can ever actually be invalid.
        b32 screen_dimension_is_valid = win32_screen_dimension_is_valid(screen_dimension);
        b32 screen_dimensions_are_equal = win32_compare_screen_dimensions(
            screen_dimension, last_screen_dimension);
        if (screen_dimension_is_valid && !screen_dimensions_are_equal)
        {
            Event *event = push_event(&event_buffer);

            event->type = RESIZE_EVENT;
            event->screen.width = screen_dimension.width;
            event->screen.height = screen_dimension.height;

            last_screen_dimension = screen_dimension;
        }
        win32_process_console_events(console_in, &event_buffer, &running);
        assert(event_buffer.count < MAX_EVENTS);

        pager_update(
            permanent_memory, &event_buffer,
            last_screen_dimension, &running, argc, argv);

        event_buffer.count = 0;

        f32 seconds_elapsed_so_far = win32_get_seconds_elapsed(
            last_timestamp, win32_get_timestamp());
        if (seconds_elapsed_so_far < target_seconds_per_frame)
        {
            f32 seconds_to_sleep  = target_seconds_per_frame - seconds_elapsed_so_far;
            win32_sleep(seconds_to_sleep);
        }

        LONGLONG end_timestamp = win32_get_timestamp();
        seconds_elapsed_so_far = win32_get_seconds_elapsed(
            last_timestamp, end_timestamp);

        assert(seconds_elapsed_so_far >= target_seconds_per_frame);

        last_timestamp = end_timestamp;
    }

    SetConsoleOutputCP(prev_code_page);

exit:
    timeEndPeriod(1);
    return 0;
}
