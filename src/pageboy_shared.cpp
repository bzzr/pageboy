#define ESC_SEQUENCE_SWITCH_TO_MAIN_BUFFER "\x1b[?1049l"
#define ESC_SEQUENCE_SWITCH_TO_ALT_BUFFER  "\x1b[?1049h"
#define ESC_SEQUENCE_SHOW_CURSOR           "\x1b[?25h"
#define ESC_SEQUENCE_HIDE_CURSOR           "\x1b[?25l"
#define ESC_SEQUENCE_MOVE_FIRST_LINE       "\x1b[0;0H"
#define ESC_SEQUENCE_ERASE_IN_LINE         "\x1b[K"
#define ESC_SEQUENCE_CURSOR_NEXT_LINE      "\x1b[1E"
#define ESC_SEQUENCE_ERASE_SCROLLBACK      "\x1b[3J"
#define ESC_SEQUENCE_SGR_DEFAULT           "\x1b[0m"


enum HID_Code;

enum Keyboard_Event_Type
{
    KEYBOARD_EVENT_KEYDOWN,
    KEYBOARD_EVENT_KEYUP
};

struct Keyboard_Event
{
    Keyboard_Event_Type type;
    HID_Code hid_code;
};

struct Resize_Event
{
    u32 width;
    u32 height;
};

enum Event_Type
{
    KEYBOARD_EVENT,
    RESIZE_EVENT
};

struct Event
{
    Event_Type type;
    union
    {
        Keyboard_Event key;
        Resize_Event screen;
    };
};

// @Note: Maybe change these to unsigned?
struct Screen_Dimension
{
    u32 width;
    u32 height;
};

struct Buffer
{
    u8 *data;
    uma count;
};

typedef Buffer String;


// Platform API

#define PLATFORM_ALLOC(name) void *name(uma size)
typedef PLATFORM_ALLOC(fn_platform_alloc);

#define PLATFORM_RESERVE_MEMORY(name) void *name(uma size)
typedef PLATFORM_RESERVE_MEMORY(fn_platform_reserve_memory);

#define PLATFORM_COMMIT_MEMORY(name) void *name(void *address, uma size)
typedef PLATFORM_COMMIT_MEMORY(fn_platform_commit_memory);

#define PLATFORM_WRITE_TO_STDOUT(name) void name(char *string, uma size)
typedef PLATFORM_WRITE_TO_STDOUT(fn_platform_write_to_stdout);

#define PLATFORM_PANIC(name) void name(char *message)
typedef PLATFORM_PANIC(fn_platform_panic);

#define PLATFORM_GET_FILE_SIZE(name) int name(char *path, u32 *size)
typedef PLATFORM_GET_FILE_SIZE(fn_platform_get_file_size);

#define PLATFORM_READ_FILE(name) int name(char *path, u32 size, void *memory)
typedef PLATFORM_READ_FILE(fn_platform_read_file);

#define PLATFORM_WRITE_FILE(name) int name(char *path, u32 size, void *memory)
typedef PLATFORM_WRITE_FILE(fn_platform_write_file);

#define PLATFORM_ABSOLUTE_PATH(name) int name(char *path, char **absolute_path)
typedef PLATFORM_ABSOLUTE_PATH(fn_platform_absolute_path);

struct Platform
{
    fn_platform_alloc           *alloc;
    fn_platform_reserve_memory  *reserve_memory;
    fn_platform_commit_memory   *commit_memory;
    fn_platform_write_to_stdout *write_to_stdout;
    fn_platform_panic           *panic;
    fn_platform_get_file_size   *get_file_size;
    fn_platform_read_file       *read_file;
    fn_platform_write_file      *write_file; // @Incomplete: Remove this
    fn_platform_absolute_path   *absolute_path;

    uma page_size;
    uma allocation_granularity;

    String linesep;
};


// Pageboy API

#define PAGEBOY_UPDATE() void pager_update(void *permanent_memory, Buffer *events, Screen_Dimension screen, int *running, u32 argc, char **argv)
PAGEBOY_UPDATE();
