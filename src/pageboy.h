// @Todo: Add tests for utf8, aes and gpb.
// @Todo: Make sure long paragraphs work.
// @Todo: Rename chunks to paragraphs.
// @Todo: We are declaring too many uma variables. Investigate further.
// @Todo: Remove all references to pageboy code from the platform layer
// except for the stuff in pageboy_shared.cpp
// @Todo: Stream large text files.
// @Todo: Document the code where necessary.
// @Todo: Intercept CTRL+C.
// @Todo: Scroll horizontally.
// @Todo: Add option to remove superfluous spaces.
// @Todo: Word wrapping.
// @Todo: Searching.
// @Todo: Support different encodings.
// @Todo: Add option to interpret ansi escape sequences literally.
// @Todo: Use String instead of Buffer where appropriate.
// @Todo: Move large tables to beginning of the file.
// @Todo: next_grapheme_break() will never return a pointer between
// a CR and a LF, so we might not need linesep_inc() anymore. Instead,
// we could simply check that we have a newline.

struct Chunk
{
    u8 *data;
    uma total_count;
    uma grclu_count;
};

struct Position
{
    uma chunk_index;
    uma vertical_offset;
};

enum Move_Unit
{
    MOVE_ROW,
    MOVE_PAGE,
};

struct Pager_State
{
    Memory_Arena text_arena;
    Buffer text;

    Memory_Arena backbuffer_arena;
    String backbuffer;

    Memory_Arena chunk_arena;
    Buffer chunks;

    Position position;
    Screen_Dimension screen;

    b32 dirty;
    b32 initialized;
};
