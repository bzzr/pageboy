#include "pageboy_first.cpp"
#include "pageboy_shared.cpp"

extern Platform platform;

#include "pageboy_utils.cpp"
#include "pageboy_memory.cpp"
#include "pageboy_hid.cpp"
#include "pageboy_utf8.cpp"
#include "pageboy_aes.cpp"
#include "pageboy_argument_parser.cpp"
#include "pageboy.h"


internal void
remove_all_non_sgr_sequences(Memory_Arena *arena, Buffer text)
{
    Buffer at = text;
    while (at.count)
    {
        b32 is_sgr;
        char *after = aes_inc((char *)at.data, &is_sgr);

        b32 is_aes = false;
        if ((u8 *)after > at.data)
        {
            is_aes = true;
        }
        else
        {
            after++;
        }

        uma bytes_ahead = (u8 *)after - at.data;
        if (!is_aes || is_sgr)
        {
            u8 *out = push_array(arena, u8, bytes_ahead, 1);
            copy_memory(out, at.data, bytes_ahead);
        }

        at = advance_buffer(at, bytes_ahead);
    }
}

internal void 
get_chunks(Memory_Arena *arena, Buffer text, u32 max_chunk_size, Buffer *chunks)
{
    Chunk *chunk = push_to_buffer(arena, chunks, Chunk);
    chunk->data = text.data;

    b32 chunk_too_long = false;
    b32 is_linesep     = false;

    char *at  = (char *)text.data;
    char *end = (char *)offset_by_bytes(text.data, text.count);
    while (at < end)
    {
        if (chunk_too_long || is_linesep)
        {
            chunk = push_to_buffer(arena, chunks, Chunk);
            chunk->data = (u8 *)at;
            chunk_too_long = false;
            is_linesep     = false;
        }

        char *tmp = 0;
        uma bytes_ahead = 0;
        uma grclu_ahead = 0;

        tmp = aes_inc(at);
        bytes_ahead = tmp - at;
        if (bytes_ahead > 0) goto after;

        tmp = linesep_inc(at);
        bytes_ahead = tmp - at;
        is_linesep = bytes_ahead > 0;
        if (is_linesep) goto after;

        tmp = next_grapheme_break(at);
        bytes_ahead = tmp - at;
        grclu_ahead = 1;

after:
        chunk_too_long = (chunk->total_count + bytes_ahead > max_chunk_size);
        if (!chunk_too_long)
        {
            chunk->total_count += bytes_ahead;
            chunk->grclu_count += grclu_ahead;
        }

        at += bytes_ahead;
    }
}

internal inline s32
compare_positions(Position pos1, Position pos2)
{
    if (pos1.chunk_index > pos2.chunk_index)
    {
        return 1;
    }
    else if (pos1.chunk_index < pos2.chunk_index)
    {
        return -1;
    }
    else
    {
        if (pos1.vertical_offset > pos2.vertical_offset)
        {
            return 1;
        }
        else if (pos1.vertical_offset < pos2.vertical_offset)
        {
            return -1;
        }
        else
        {
            return 0;
        }
    }
}

internal inline u32
calc_line_count(uma grclu_count, s32 screen_width)
{
    u32 result = maximum((((s32)grclu_count - 1) / screen_width) + 1, 1);
    return result;
}

internal Position
step_position(Buffer *chunks, Screen_Dimension screen, Position pos, s32 direction)
{
    assert(direction == 1 || direction == -1);

    uma chunk_index     = pos.chunk_index;
    uma vertical_offset = pos.vertical_offset;

    Chunk *chunk = offset_by_count(chunks->data, Chunk, chunk_index);
    u32 screen_line_count = calc_line_count(chunk->grclu_count, (u32)screen.width);

    uma next_vertical_offset = vertical_offset + direction;
    if (next_vertical_offset >= 0 && next_vertical_offset < screen_line_count)
    {
        vertical_offset = next_vertical_offset;
    }
    else
    {
        chunk_index += direction;
        if (chunk_index >= 0 && chunk_index < chunks->count)
        {
            chunk = offset_by_count(chunks->data, Chunk, chunk_index);
            screen_line_count = calc_line_count(chunk->grclu_count, (u32)screen.width);
            vertical_offset = (direction > 0) ? 0 : ((s32)screen_line_count - 1);
        }
        else if (chunk_index == 0 || chunk_index == chunks->count)
        {
            vertical_offset = 0;
        }
        else
        {
            assert(0);
        }
    }

    Position result;
    result.chunk_index     = chunk_index;
    result.vertical_offset = vertical_offset;
    return result;
}

internal u32
step_and_count(Buffer *chunks, Screen_Dimension screen, u32 max)
{
    u32 result = 0;

    Position position = {};
    while (position.chunk_index < chunks->count && result < max)
    {
        position = step_position(chunks, screen, position, 1);
        result++;
    }

    return result;
}

internal Position
get_max_position(Buffer *chunks, Screen_Dimension screen)
{
    Position result = {};
    if (step_and_count(chunks, screen, screen.height+1) <= screen.height)
    {
        return result;
    }

    uma chunk_index = chunks->count - 1;
    Chunk *chunk = offset_by_count(chunks->data, Chunk, chunk_index);
    u32 line_count = calc_line_count(chunk->grclu_count, (u32)screen.width);
    s32 vertical_offset = (s32)line_count - 1;

    result.chunk_index     = chunk_index;
    result.vertical_offset = vertical_offset;

    for (u32 i = 0; i < screen.height-1; i++)
    {
        result = step_position(chunks, screen, result, -1);
    }

    return result;
}

internal Position
move_position(Buffer *chunks, Position position, Screen_Dimension screen, s32 row_offset, Move_Unit unit)
{
    s32 displacement = row_offset;
    if (unit == MOVE_PAGE)
    {
        displacement = row_offset * screen.height;
    }

    s32 magnitude = displacement;
    s32 direction = 1;
    if (displacement < 0)
    {
        magnitude = -displacement;
        direction = -1;
    }

    Position pos = position;
    Position min_pos = {};
    Position max_pos = get_max_position(chunks, screen);
    for (s32 i = 0; i < magnitude; i++)
    {
        if ((direction < 0 && !compare_positions(pos, min_pos)) ||
             direction > 0 && !compare_positions(pos, max_pos))
        {
            break;
        }

        pos = step_position(chunks, screen, pos, direction);
    }

    return pos;
}

internal char *
advance_one_screen_line(char *string, u32 screen_width)
{
    uma grclu_count = 0;
    char *at = string;
    while (*at)
    {
        char *tmp = aes_inc(at);
        if (tmp > at)
        {
            at = tmp;
            continue;
        }

        tmp = linesep_inc(at);
        if (tmp > at) break;

        tmp = next_grapheme_break(at);
        if (grclu_count + 1 > screen_width)
        {
            break;
        }
        grclu_count++;
        at = tmp;
    }

    return at;
}

internal char *
advance_one_screen_line_and_consume_sgr(
    char *string,
    u32 screen_width,
    SGR_State *sgr_state)
{
    uma grclu_count = 0;
    char *at = string;
    while (*at)
    {
        b32 is_sgr;
        char *tmp = aes_inc(at, &is_sgr);
        if (is_sgr) sgr_consume(sgr_state, at);
        if (tmp > at)
        {
            at = tmp;
            continue;
        }

        tmp = linesep_inc(at);
        if (tmp > at) break;

        tmp = next_grapheme_break(at);
        if (grclu_count + 1 > screen_width)
        {
            break;
        }
        grclu_count++;
        at = tmp;
    }

    return at;
}

internal inline void
append_string_safe(Memory_Arena *arena, String *backbuffer, String string)
{
    while (string.count)
    {
        char *at = (char *)string.data;

        u32 cp;
        b32 valid;
        char *tmp = utf8_inc(at, &cp, &valid);
        uma bytes_ahead = tmp - at;

        if (valid)
        {
            append_string(arena, backbuffer, at, bytes_ahead);
        }
        else
        {
            append_string(arena, backbuffer, "\xEF\xBF\xBD");
        }
        string = advance_buffer(string, bytes_ahead);
    }
}

internal void
fill_backbuffer(
    Memory_Arena     *arena,
    Buffer           *chunks,
    Position          position,
    Screen_Dimension  screen,
    String           *backbuffer)
{
    Chunk *top_chunk = offset_by_count(chunks->data, Chunk, position.chunk_index);
    char *at = (char *)top_chunk->data;

    SGR_State sgr_state = {};
    for (u32 i = 0; i < position.vertical_offset; i++)
    {
        at = advance_one_screen_line_and_consume_sgr(at, screen.width, &sgr_state);
    }

    // @Note: Whenever the window is resized on Windows Terminal, the viewport
    // might be scrolled due to the cursor being pushed off-screen. Because of
    // this we try to erase the scrollback before updating the screen's
    // contents but, for now, it does not seem to work.
    append_string(arena, backbuffer, ESC_SEQUENCE_ERASE_SCROLLBACK);
    append_string(arena, backbuffer, ESC_SEQUENCE_MOVE_FIRST_LINE);
    
    // @Note: What should we do if sgr code is bigger than the buffer?
    char sgr_code[128] = {};
    sgr_get_code(&sgr_state, sgr_code);
    append_string(arena, backbuffer, sgr_code);

    u32 screen_line_count = step_and_count(chunks, screen, screen.height);
    for (u32 i = 0; i < screen_line_count; i++)
    {
        append_string(arena, backbuffer, ESC_SEQUENCE_ERASE_IN_LINE);

        char *tmp1 = advance_one_screen_line(at, screen.width);
        char *tmp2 = linesep_inc(tmp1);
        uma linesep_length = tmp2 - tmp1;

        b32 doing_last_screen_line = i == (screen_line_count-1);

        String line;
        line.data  = (u8 *)at;
        line.count = doing_last_screen_line ? (tmp1 - at) : (tmp2 - at);
        append_string_safe(arena, backbuffer, line);

        if (!doing_last_screen_line && !linesep_length)
        {
            append_string(arena, backbuffer, ESC_SEQUENCE_CURSOR_NEXT_LINE);
        }

        at = tmp2;
    }

    append_string(arena, backbuffer, ESC_SEQUENCE_SGR_DEFAULT);
}

internal void
process_events(Pager_State *state, Buffer *events, int *running)
{
    Position new_position = state->position;
    for (u32 i = 0; i < events->count; i++)
    {
        Event *event = offset_by_count(events->data, Event, i);
        switch (event->type)
        {
            case KEYBOARD_EVENT:
            {
                switch (event->key.type)
                {
                    case KEYBOARD_EVENT_KEYDOWN:
                    {
                        switch (event->key.hid_code)
                        {
                            case HID_CODE_UP_ARROW:
                            {
                                new_position = move_position(
                                    &state->chunks, state->position, state->screen, -1, MOVE_ROW);
                            } break;
                            case HID_CODE_DOWN_ARROW:
                            {
                                new_position = move_position(
                                    &state->chunks, state->position, state->screen, 1, MOVE_ROW);
                            } break;
                            case HID_CODE_PAGE_UP:
                            {
                                new_position = move_position(
                                    &state->chunks, state->position, state->screen, -1, MOVE_PAGE);
                            } break;
                            case HID_CODE_PAGE_DOWN:
                            {
                                new_position = move_position(
                                    &state->chunks, state->position, state->screen, 1, MOVE_PAGE);
                            } break;
                            case HID_CODE_HOME:
                            {
                                zero_struct(&new_position);
                            } break;
                            case HID_CODE_END:
                            {
                                new_position = get_max_position(&state->chunks, state->screen);
                            } break;
                            case HID_CODE_Q:
                            {
                                platform.write_to_stdout(ESC_SEQUENCE_SWITCH_TO_MAIN_BUFFER, 0);
                                platform.write_to_stdout(ESC_SEQUENCE_SHOW_CURSOR, 0);
                                *running = false;
                            } break;
                        }
                        state->dirty = compare_positions(state->position, new_position) != 0;
                    } break;
                }
            } break;
            case RESIZE_EVENT:
            {
                Buffer *chunks = &state->chunks;
                uma chunk_index = new_position.chunk_index;

                Screen_Dimension new_screen = {event->screen.width, event->screen.height};

                Chunk *chunk = offset_by_count(chunks->data, Chunk, chunk_index);
                uma last_line_index = calc_line_count(chunk->grclu_count, new_screen.width) - 1;

                if (new_position.vertical_offset > last_line_index)
                {
                    new_position.vertical_offset = last_line_index;
                }

                Position max_position = get_max_position(chunks, new_screen);
                if (compare_positions(new_position, max_position) > 0)
                {
                    new_position = max_position;
                }

                state->position = new_position;
                state->screen = new_screen;
                state->dirty = true;
            } break;
        }
    }

    state->position = new_position;
}

PAGEBOY_UPDATE()
{
    Pager_State *state = (Pager_State *)permanent_memory;
    if (!state->initialized)
    {
        Argument_Parser argument_parser = {};

        char *file_path = 0;
        add_positional_parameter(&argument_parser, PARAMETER_TYPE_STRING, &file_path);

        b32 quit_if_one_screen = false;
        add_optional_parameter(&argument_parser, 0, "--quit-if-one-screen", PARAMETER_TYPE_BOOL, &quit_if_one_screen);

        parse_arguments(&argument_parser, argc-1, argv+1);

        char *absolute_path;
        if (!platform.absolute_path(file_path, &absolute_path))
        {
            platform.panic("error: could not open file\n");
        }

        u32 file_size;
        if (!platform.get_file_size(absolute_path, &file_size))
        {
            platform.panic("error: could not open file\n");
        }

        if (!file_size)
        {
            platform.write_to_stdout("error: file is empty\n", 0);
            *running = false;
            return;
        }

        void *file_memory = platform.alloc(file_size);
        if (!file_memory)
        {
            platform.panic("error: could not open file\n");
        }

        if (!platform.read_file(absolute_path, file_size, file_memory))
        {
            platform.panic("error: could not open file\n");
        }

        Buffer text;
        text.count = file_size;
        text.data  = (u8 *)file_memory;

        init_arena(&state->text_arena, terabytes(1)); // @Robustness: Update this value for 32-bits.
        remove_all_non_sgr_sequences(&state->text_arena, text);
        state->text.data  = state->text_arena.base;
        state->text.count = state->text_arena.used;

        init_arena(&state->backbuffer_arena);
        state->backbuffer.data = state->backbuffer_arena.base;

        init_arena(&state->chunk_arena, gigabytes(1));
        state->chunks.data = state->chunk_arena.base;
        // @Performance: Tweak this value.
        u32 max_chunk_size = kilobytes(1);
        get_chunks(&state->chunk_arena, state->text, max_chunk_size, &state->chunks);

        if (quit_if_one_screen && step_and_count(&state->chunks, screen, screen.height+1) <= screen.height)
        {
            platform.write_to_stdout((char *)file_memory, file_size);
            *running = false;
            return;
        }

        state->screen = screen;

        platform.write_to_stdout(ESC_SEQUENCE_SWITCH_TO_ALT_BUFFER, 0);
        platform.write_to_stdout(ESC_SEQUENCE_HIDE_CURSOR, 0);

        fill_backbuffer(
            &state->backbuffer_arena, &state->chunks,
            state->position, state->screen, &state->backbuffer);

#if 1
        platform.write_to_stdout(
            (char *)state->backbuffer.data, state->backbuffer.count);
#endif

#if 0
    for (u32 i = 0; i < state->backbuffer.count; i++)
    {
        platform.write_to_stdout((char *)&state->backbuffer.data[i], 1);
    }
#endif

        state->initialized = true;
    }

    process_events(state, events, running);
    if (state->dirty)
    {
        state->backbuffer.count = 0;
        reset_arena(&state->backbuffer_arena);

        fill_backbuffer(
            &state->backbuffer_arena, &state->chunks,
            state->position, state->screen, &state->backbuffer);
        platform.write_to_stdout(
            (char *)state->backbuffer.data, state->backbuffer.count);

        state->dirty = false;
    }
}
