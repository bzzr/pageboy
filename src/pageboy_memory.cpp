const u32 DEFAULT_ALIGNMENT = 8;


internal void
zero_memory(void *base, uma size)
{
    u8 *byte = (u8 *)base;
    while (size--)
    {
        *byte++ = 0;
    }
}

#define zero_struct(x) zero_memory(x, sizeof(*(x)));

internal void *
copy_memory(void *dest, void *source, uma size)
{
    u8 *source_at = (u8 *)source;
    u8 *dest_at = (u8 *)dest;
    while(size--)
    {
        *dest_at++ = *source_at++;
    }
    return dest_at;
}

struct Memory_Arena
{
    u8 *base;
    uma reserved;
    uma size;
    uma used;
    b32 is_fixed_size;
};

enum Memory_Arena_Flags
{
    MEMORY_ARENA_FIXED_SIZE    = 1 << 0,
    MEMORY_ARENA_CLEAR_TO_ZERO = 1 << 1
};

internal void
init_arena(Memory_Arena *arena, uma reserve_size = 0)
{
    reserve_size = round_up(
        else_default(reserve_size, platform.allocation_granularity),
        platform.allocation_granularity);

    void *memory = platform.reserve_memory(reserve_size);
    if (memory)
    {
        arena->base = (u8 *)memory;
        arena->reserved = reserve_size;
        arena->size = 0;
        arena->used = 0;
        arena->is_fixed_size = false;
    }
    else
    {
        platform.panic("Not enough memory.\n");
    }
}

internal void
init_arena_fs(Memory_Arena *arena, uma size)
{
    assert(size)

    void *memory = platform.alloc(size);
    if (memory)
    {
        arena->base = (u8 *)memory;
        arena->reserved = size;
        arena->size = size;
        arena->used = 0;
        arena->is_fixed_size = true;
    }
    else
    {
        platform.panic("Not enough memory.\n");
    }
}

internal void
init_arena_fs(Memory_Arena *arena, void *base, uma size)
{
    assert(size);

    zero_struct(arena);
    arena->base = (u8 *)base;
    arena->reserved = size;
    arena->size = size;
    arena->is_fixed_size = true;
}

internal inline void
reset_arena(Memory_Arena *arena)
{
    arena->used = 0;
}

internal void *
push_size(Memory_Arena *arena, uma size, u32 alignment = 0, u32 flags = 0)
{
    assert(size);

    alignment = else_default(alignment, DEFAULT_ALIGNMENT);
    assert(is_pow2(alignment));

    uma alignment_mask = alignment - 1;
    uma alignment_offset = 0;

    u8 *end = arena->base + arena->used;
    uma end_address = (uma)end;
    if (end_address & alignment_mask)
    {
        alignment_offset = alignment - (end_address & alignment_mask);
    }

    uma adjusted_size = size + alignment_offset;
    uma new_used = arena->used + adjusted_size;

    if (new_used > arena->size)
    {
        if ((new_used <= arena->reserved) && !arena->is_fixed_size)
        {
            uma allocation_size = round_up(size, platform.page_size);
            if (platform.commit_memory(end, allocation_size))
            {
                arena->size += allocation_size;
            }
            else
            {
                platform.panic("Not enough memory.\n");
            }
        }
        else
        {
            platform.panic("Not enough memory.\n");
        }
    }

    arena->used = new_used;

    void *result = offset_by_bytes(end, alignment_offset);
    if (flags & MEMORY_ARENA_CLEAR_TO_ZERO)
    {
        zero_memory(result, adjusted_size);
    }

    return result;
}

#define push_type(arena, type, ...) \
    (type *)push_size(arena, sizeof(type), ## __VA_ARGS__)

#define push_array(arena, type, count, ...) \
    (type *)push_size(arena, (count)*sizeof(type), ## __VA_ARGS__)

#define push_copy(arena, source, size, ...) \
    copy_memory(push_size(arena, size, ## __VA_ARGS__), source, size)

#define push_to_buffer(arena, buffer, type, ...) \
    ((buffer)->count++, push_type(arena, type, ## __VA_ARGS__))


internal String
push_string(Memory_Arena *arena, char *source, u32 alignment = 0, u32 flags = 0)
{
    String result;
    result.count = string_length(source);
    result.data = (u8 *)push_copy(arena, source, result.count);
    return result;
}

internal inline void
append_string(Memory_Arena *arena, String *string, char *source, uma source_count)
{
    if (source_count)
    {
        push_copy(arena, source, source_count, 1, 0);
        string->count += source_count;
    }
}

internal inline void
append_string(Memory_Arena *arena, String *string, char *source)
{
    uma source_count = string_length(source);
    append_string(arena, string, source, source_count);
}

internal inline Memory_Arena
sub_arena(Memory_Arena *arena, uma size)
{
    void *base = push_size(arena, size);
    Memory_Arena result;
    init_arena_fs(&result, base, size);
    return result;
}

struct Memory_Arena_State
{
    Memory_Arena *arena;
    uma used;
};

internal inline Memory_Arena_State
arena_save(Memory_Arena *arena)
{
    Memory_Arena_State result;
    result.arena = arena;
    result.used = arena->used;
    return result;
}

internal inline void
arena_restore(Memory_Arena_State state)
{
    Memory_Arena *arena = state.arena;
    assert(arena->used >= state.used);
    arena->used = state.used;
}
