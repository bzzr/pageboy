#include <stdint.h>

typedef int8_t    s8;
typedef int16_t   s16;
typedef int32_t   s32;
typedef int64_t   s64;
typedef s32       b32;
typedef uint8_t   u8;
typedef uint16_t  u16;
typedef uint32_t  u32;
typedef uint64_t  u64;
typedef float     f32;
typedef double    f64;
typedef intptr_t  sma;
typedef uintptr_t uma;

#define internal      static
#define global        static
#define local_persist static

#if PAGEBOY_DEBUG
#define assert(expression) if (!(expression)) { *(int *)0 = 0; }
#else
#define assert(expression)
#endif

#define kilobytes(value) ((value)*1024LL)
#define megabytes(value) (kilobytes(value)*1024LL)
#define gigabytes(value) (megabytes(value)*1024LL)
#define terabytes(value) (gigabytes(value)*1024LL)

#define maximum(x, y) (((x) > (y)) ? (x) : (y))
#define minimum(x, y) (((x) < (y)) ? (x) : (y))

#define offset_by_bytes(pointer, bytes) ((u8 *)(pointer) + (bytes))
#define offset_by_count(pointer, type, count) ((type *)(pointer) + (count))

#define cast_and_deref(pointer, type) *((type *)(pointer))

#define else_default(x, y) ((x) ? (x) : (y))

#define is_pow2(x) (((x) & ((x)-1)) == 0)

#define array_count(a) (sizeof(a)/sizeof(a[0]))
